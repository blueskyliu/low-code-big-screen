import cloneDeep from 'lodash/cloneDeep'
import { PublicConfigClass } from '@/packages/public'
import { CreateComponentType } from '@/packages/index.d'
import { chartInitConfig } from '@/settings/designSetting'
import { TablesBasicConfig } from './index'
import { COMPONENT_INTERACT_EVENT_KET } from '@/enums/eventEnum'
import dataJson from './data.json'
import { interactActions, ComponentInteractEventEnum } from './interact'
const { dimensions, source } = dataJson
export const option = {
  dataset: { dimensions, source },
    // 时间组件展示类型，必须和 interactActions 中定义的数据一致
    [COMPONENT_INTERACT_EVENT_KET]: ComponentInteractEventEnum.DATA,
  pagination: {
    page: 1,
    pageSize: 5
  },
  align: 'center',
  style: {
    border: 'on',
    singleColumn: 'off',
    singleLine: 'off',
    bottomBordered: 'on',
    striped: 'on',
    fontSize: 16,
    borderWidth: 0,
    borderColor: 'black',
    borderStyle: 'solid'
  },
  inputShow: 'none'
}

export default class Config extends PublicConfigClass implements CreateComponentType {
  public key = TablesBasicConfig.key
  public attr = { ...chartInitConfig, w: 600, h: 300, zIndex: -1 }
  public chartConfig = cloneDeep(TablesBasicConfig)
  public interactActions = interactActions
  public option = cloneDeep(option)
}
