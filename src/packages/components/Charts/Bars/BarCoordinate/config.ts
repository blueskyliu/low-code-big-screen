import { echartOptionProfixHandle, PublicConfigClass } from '@/packages/public'
import { BarCoordinateConfig } from './index'
import { CreateComponentType } from '@/packages/index.d'
import cloneDeep from 'lodash/cloneDeep'
import dataJson from './data.json'

export const includes = ['legend', 'xAxis', 'yAxis', 'grid']
export const seriesItem = {
  type: 'bar',
  barWidth: 15,
  label: {
    show: true,
    position: 'top',
    color: '#fff',
    fontSize: 12
  },
  coordinateSystem: 'polar',
  itemStyle: {
    color: null,
    borderRadius: 2
  }
}
export const option = {
  tooltip: {
    show: true,
    trigger: 'axis',
    axisPointer: {
      show: true,
      type: 'shadow'
    }
  },
  polar: {
    radius: [30, '80%']
  },
  angleAxis: {
    max: 4,
    startAngle: 75
  },
  radiusAxis: {
    type: 'category',
    data: dataJson.source.map(res=>res.product)
  },
xAxis: {
    show: false,
    type: 'category'
  },
  yAxis: {
    show: false,
    type: 'value'
  },
  dataset: { ...dataJson },
  series: [seriesItem, seriesItem]
}

export default class Config extends PublicConfigClass implements CreateComponentType {
  public key = BarCoordinateConfig.key
  public chartConfig = cloneDeep(BarCoordinateConfig)
  // 图表配置项
  public option = echartOptionProfixHandle(option, includes)
}
