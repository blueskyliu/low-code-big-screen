import { ConfigType, PackagesCategoryEnum, ChartFrameEnum } from '@/packages/index.d'
// import { ChatCategoryEnum, ChatCategoryEnumName } from '../../index.d'

 const pieOption: any = {

  series: [
    {
      type: 'pie',
    },
   
  ],
  dataset:{
  "dimensions": [],
  "source": [ ]
}
}
const LineOption: any = {
  xAxis: {
    type: 'category',
    data: []
  },
  yAxis: {
    type: 'value'
  },
  series: [
    {
      type: 'line',
    },
  ],
  dataset:{
  "dimensions": [],
  "source": [ ]
}
}
const BarOption: any = {
  xAxis: {
    type: 'category',
    data: []
  },
  yAxis: {
    type: 'value'
  },
  series: [
    {
      type: 'bar',
    },
  ],
  dataset:{
  "dimensions": [],
  "source": [ ]
}
}
const TableOption: any = {

  series: [
    {
      type: 'pie',
    },
   
  ],
  dataset:{
  "dimensions": [],
  "source": [ ]
}
}

export default {
  pieOption,BarOption,LineOption
}