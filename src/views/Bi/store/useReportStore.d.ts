



import type { Data,Fields,Meta,SortParam } from '@antv/s2'
// 数据库表 Data,Fields,Meta,SortParam 
// 基础事件类型(vue不加 on)
export enum DataBaseKey {
    // 名字
    NAME = 'name',
    // 数据集 唯一id
    ID = 'id',
    // FIELDS 功能描述： 配置表格的维度域，即对应行列维度
    FIELDS = 'fields',
    // 设置表的数据源数据源
    DATA = 'data',
    // 字段元数据，可配置字段别名和数值格式化。
    META = 'meta'
  }
export interface DataBase {
    name:string
    id:string | number
    fields:Fields
    data:Data[]
    meta:Meta[]
  }
export interface BaseStyles {
    x:number,
    y:number,
    w:number,
    h:number,
    initW:number,
    initH:number,
}
  //组件数据v
export interface ComponentType {
    options:DataBase
    styles:BaseStyles,
    type: string,//组件类型
}
//仪表盘数据
export interface InstrumentType {
    data:ComponentType[]
}
//分析表 
export interface BiReportData {
    type: string,  //模块类型
    data: InstrumentType| ComponentType | Object,
    name: string,
    close: boolean, //true 可以删除 false 不可以删除
    id: string
}

export interface UseNavStoreType {
    database:DataBase[]
    targetId:string
    //当前分析数据
    biReportData:BiReportData[],
    targetDataBase:DataBase | null
}

