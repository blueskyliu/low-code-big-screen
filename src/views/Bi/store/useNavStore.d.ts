export enum UseNavStoreTypeEnums {
  // 数据分析导航 面包屑
  DATA_ANALYSIS_NAVIGATION = 'dataAnalysisNavigation',
  DEFAULT_SELECTED_KEYS='defaultSelectedKeys',
  DEFAULT_EXPANDED_KEYS="defaultExpandedKeys",
}

export interface UseNavStoreType {
  [UseNavStoreTypeEnums.DATA_ANALYSIS_NAVIGATION]: any;
  [UseNavStoreTypeEnums.DEFAULT_SELECTED_KEYS]: string[];
  [UseNavStoreTypeEnums.DEFAULT_EXPANDED_KEYS]: string[];
}
