import { defineStore } from 'pinia'
import { UseNavStoreType } from './useNavStore.d'

export const useNavStore = defineStore({
  id: 'useBiNavStore',
  state: (): UseNavStoreType => {
    return {
      dataAnalysisNavigation:[],
      defaultSelectedKeys: [],
      defaultExpandedKeys: [],
    }
  },
  getters: {
    getdataAnalysisNavigation(): any {
      return this.dataAnalysisNavigation
    },
    getdefaultSelectedKeys(): any {
      return this.defaultSelectedKeys
    },
    getdefaultExpandedKeys(): any {
      return this.defaultExpandedKeys
    },
  },
  actions: {
    setdataAnalysisNavigation(data:any): void {
      this.dataAnalysisNavigation = data;
    },
    setdefaultSelectedKeys(data:any): void {
      this.defaultSelectedKeys = data;
    },
    setdefaultExpandedKeys(data:any): void {
      this.defaultExpandedKeys = data;
    }
  }
})
