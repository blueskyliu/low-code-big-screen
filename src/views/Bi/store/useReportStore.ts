import { defineStore } from 'pinia'
import { TabsBottomType, TabsBottomTypeName } from '@/enums/editReportEnum'
import { getUUID } from '@/utils'
import { DataBase, UseNavStoreType } from './useReportStore.d'
export const useReportStore = defineStore({
  id: 'useBiNavStore',
  state: (): UseNavStoreType => {
    return {
      database: [
        {
          name: '访问统计',
          id: getUUID(),
          fields: {
            columns: [],
            values:[],
            rows:[]
          },
          data: [
            {
              province: '浙江',
              city: '杭州',
              type: '家具',
              sub_type: '桌子',
              price: '1',
              province1: '浙江',
              city1: '杭州',
              type1: '家具',
              sub_type1: '桌子',
              price1: '1'
            },
            {
              province: '浙江',
              city: '杭州',
              type: '家具',
              sub_type: '沙发',
              price: '2',

              province1: '浙江',
              city1: '杭州',
              type1: '家具',
              sub_type1: '桌子',
              price1: '1'
            },
            {
              province: '浙江',
              city: '杭州',
              type: '办公用品',
              sub_type: '笔',
              price: '3',
              province1: '浙江',
              city1: '杭州',
              type1: '家具',
              sub_type1: '桌子',
              price1: '1'
            },
            {
              province: '浙江',
              city: '杭州',
              type: '办公用品',
              sub_type: '纸张',
              price: '4',
              province1: '浙江',
              city1: '杭州',
              type1: '家具',
              sub_type1: '桌子',
              price1: '1'
            }
          ],
          meta: [
            // 列头字段对应的元信息，比如展示的中文名
            {
              field: 'province',
              name: '省份'
            },
            {
              field: 'city',
              name: '城市'
            },
            {
              field: 'type',
              name: '类型'
            },
            {
              field: 'price',
              name: '价格'
            }
          ]
        },
        {
          name: '财务统计',
          id: getUUID(),
          fields: {
            columns: [],
            values:[],
            rows:[]
          },
          meta: [
            // 列头字段对应的元信息，比如展示的中文名
            {
              field: 'province',
              name: 'C省份'
            },
            {
              field: 'city',
              name: 'C城市'
            },
            {
              field: 'type',
              name: 'C类型'
            },
            {
              field: 'price',
              name: '价格'
            },
            {
              field: 'sub_type',
              name: '原价'
            }
          ],
          data: [
            {
              province: '浙江',
              city: '杭州',
              type: '家具',
              sub_type: '3',
              price: '1'
            },
            {
              province: '浙江',
              city: '北京',
              type: '家具',
              sub_type: '4',
              price: '2'
            },
            {
              province: '浙江',
              city: '上海',
              type: '办公用品',
              sub_type: '10',
              price: '3'
            },
            {
              province: '浙江',
              city: '深圳',
              type: '办公用品',
              sub_type: '6',
              price: '4'
            }
          ]
        }
      ],
      // 当前激活模块
      targetId: '',
      biReportData: [
        {
          type: TabsBottomType.DATA,
          data: {},
          name: TabsBottomTypeName.DATA,
          close: false, //true 可以删除 false 不可以删除
          id: getUUID()
        },
        {
          type: TabsBottomType.COMPONENT,
          data:{
            options:{},
            styles:{
              x:0,
              y:0,
              w:480,
              h:300,
              initW:480,
              initH:300,
            }
          },
          name: TabsBottomTypeName.COMPONENT,
          close: true, //true 可以删除 false 不可以删除
          id: getUUID()
        },
        {
          type: TabsBottomType.INSTRUMENT,
          data:{
            data:[],
          },
          name: TabsBottomTypeName.INSTRUMENT,
          close: true, //true 可以删除 false 不可以删除
          id: getUUID()
        }
      ],
      targetDataBase:null
    }
  },
  getters: {
    getTargetDataBase(): DataBase | null {
      return this.targetDataBase
    },
    getbiReportData(): any {
      return this.biReportData
    },
    getDataBase(): any {
      return this.database
    }
  },
  actions: {
        //更新标签页组件 数据
        // setBiReportData(data:any){
        //   this.biReportData.forEach(res=>{
        //     if(res.id === data.id){
        //       res = data
        //     }
        //   })
        // },

         // * 设置 TargetDataBase 数据项
        setTargetDataBase(data:any) {
          this.targetDataBase = data
        },
    setTargetId(id:string){
      this.targetId = id
    },
    fetchTargetIndex(id?: string): number {
      const targetId = id?id:this.targetId 
      if (!targetId) {
        return -1
      }
      const targetIndex = this.biReportData.findIndex(e => e.id === targetId)

      // 当前
      if (targetIndex !== -1) {
        return targetIndex
      }
      return -1
    },
    addtbiReportData(data: any): void {
      this.biReportData.push(data)
    },
    close(index: number): void {
      this.biReportData.splice(index, 1)
    }
  }
})
