import {
    useThemeVars
    } from 'naive-ui'

export const useBiUtils = () =>{
    const themeVars = useThemeVars()
    const useThemeS2Table = ()=>{
        return  {
            background: {
              color:  themeVars.value.bodyColor,
            },
            cornerCell: {
              cell: {
                horizontalBorderColor:  themeVars.value.borderColor,
                verticalBorderColor:  themeVars.value.borderColor,
                padding: {
                  top: 12,
                  right: 8,
                  bottom: 12,
                  left: 8,
                },
                backgroundColor:  themeVars.value.bodyColor,
              },
              text: {
                fill: themeVars.value.textColor1,
              },
              bolderText: {
                fill:themeVars.value.textColor1,
                opacity: 0.4,
              },
            },
            resizeArea:{
              guideLineColor:themeVars.value.primaryColor,
            background:  themeVars.value.primaryColor,
            },
            splitLine: {
              
              horizontalBorderColor:  themeVars.value.borderColor,
              horizontalBorderColorOpacity: 1,
              horizontalBorderWidth: 2,
              verticalBorderColor:  themeVars.value.borderColor,
              verticalBorderColorOpacity: 1,
              verticalBorderWidth: 2,
              showRightShadow: true,
              shadowWidth: 10,
              shadowColors: {
                left: themeVars.value.baseColor,
                right: themeVars.value.baseColor,
              },
            },
            colCell: {
              cell: {
                horizontalBorderColor:  themeVars.value.borderColor,
                verticalBorderColor:  themeVars.value.borderColor,
                verticalBorderWidth: 2,
                horizontalBorderWidth: 2,
                padding: {
                  top: 12,
                  right: 8,
                  bottom: 12,
                  left: 8,
                },
                backgroundColor:  themeVars.value.bodyColor,
                interactionState: {
                  hover: {
                    backgroundColor:  themeVars.value.cardColor,
                    backgroundOpacity: 1,
                  },
                  selected: {
                    backgroundColor:  themeVars.value.actionColor,
                  },
                },
              },
              text: {
                fill: themeVars.value.textColor1,
              },
                 icon: {
                fill: themeVars.value.textColor1,
              },
              bolderText: {
                fill:themeVars.value.textColor1,
                opacity: 1,
              },
            },
            
            dataCell: {
              icon: {
                size: 14,
                margin: {
                  left: 10,
                },
              },
              cell: {
                interactionState: {
                  hover: {
                    backgroundColor:  themeVars.value.cardColor,
                    backgroundOpacity: 1,
                  },
                  hoverFocus: {
                    backgroundColor:  themeVars.value.cardColor,
                    backgroundOpacity: 1,
                    borderColor: themeVars.value.textColor1,
                  },
                  selected: {
                    backgroundColor:  themeVars.value.cardColor,
                    backgroundOpacity: 1,
                  },
                  unselected: {
                    backgroundOpacity: 1,
                    opacity: 1,
                  },
                  prepareSelect: {
                    borderColor:  themeVars.value.cardColor,
                  },
                },
                horizontalBorderColor:  themeVars.value.borderColor,
                verticalBorderColor:  themeVars.value.borderColor,
                verticalBorderWidth: 2,
                horizontalBorderWidth: 2,
                padding: {
                  top: 0,
                  right: 8,
                  bottom: 2,
                  left: 0,
                },
                backgroundColorOpacity: 0.9,
                backgroundColor: themeVars.value.cardColor,
                crossBackgroundColor:  themeVars.value.tagColor,
              },
              text: {
                fill: themeVars.value.textColor1,
              },
            },
          }
    }
    return {
        useThemeS2Table
    }
}