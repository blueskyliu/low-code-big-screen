import { computed, Ref } from 'vue'
import {BiReportData } from 

'@/views/Bi/store/useReportStore.d'
import { useReportStore } from '@/views/Bi/store/useReportStore'
import { TabsBottomType } from '@/enums/editReportEnum'

// 获取当前对象数据
export const useTargetData = () => {
  const reportStore = useReportStore()
  const targetData: Ref<BiReportData> = computed(() => {
    const list = reportStore.getbiReportData
    const targetIndex = reportStore.fetchTargetIndex()
    return list[targetIndex]
  })
   //设置参数
   /**
    * key 的参数有那些
     name:string
    id:string | number
    fields:Fields
    data:Data[]
    meta:Meta[]
    */
   const setTarget = (key:string,data:any) =>{
    //获取当前选中底部数据
    const list = reportStore.getbiReportData
    const targetIndex = reportStore.fetchTargetIndex()
    const targetData:BiReportData =  list[targetIndex]
       //设置当前选择数据源
       if(targetData.type!=TabsBottomType.DATA){
        targetData.data.options[key] = data
    }
  }

  return { targetData, reportStore }
}
