 


import { RouteRecordRaw } from 'vue-router'
import { PageEnum } from '@/enums/pageEnum'
const projectRoutes: RouteRecordRaw[] = 
[
  {
    path: PageEnum.BI,
    name: PageEnum.BI_NAME,
    component:() => import('@/views/project/index.vue'),
    redirect: PageEnum.BI_HOME,
    meta: {
      title: '我的分析',
      isRoot: true
    },
    children: [
      {
        path: PageEnum.BI_HOME,
        name: PageEnum.BI_HOME_NAME,
        component: () => import('@/views/Bi/index.vue'),
        meta: {
          title: '我的项目'
        }
      }
    ]
  },
  {
    path: PageEnum.BI_Analysis,
    name: PageEnum.BI_Analysis_NAME,
    component:() => import('@/views/Bi/editReport/index.vue'),
    meta: {
      title: '分析主题',
    },

  }
]

export default projectRoutes
