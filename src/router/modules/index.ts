import projectRoutes from './project.router'
import chartRoutes from './chart.route'
import previewRoutes from './preview.route'
import editRoutes from './edit.route'
import BiRoutes from './Bi.router'
export default {
  projectRoutes,
  chartRoutes,
  previewRoutes,
  editRoutes,
  BiRoutes:BiRoutes[0],
  BiRoutesEdit:BiRoutes[1],
}