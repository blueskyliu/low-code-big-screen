// 鼠标新增 底部数据
export enum TabsBottomType {
    DATA = 'data', //数据
    COMPONENT = 'component',//组件
    INSTRUMENT = 'Instrument', //仪表盘
  }
  // 鼠标新增 底部数据名字
export enum TabsBottomTypeName {
  DATA = '数据', //数据
  COMPONENT = '组件',//组件
  INSTRUMENT = '仪表盘', //仪表盘
}
//bi报表数据类型
export enum ComponentBiDataType {
  BAR = 'bar',
  LINE = 'line',
  PIE="pie",
  TABLE = 'table',
  PIVOT_TABLE = 'PivotTable',
}

//bi报表组件类型
export enum ComponentBiType {
  COMMONCHARTS = 'commonCharts',
  TABLES2 = 'tableS2',
  
}