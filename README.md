## 功能介绍
- 独立部署
- 是否开启登陆
- 接口数据映射
- 每个图表配置文件可以独立修改

 

## 创建 git 仓库:

mkdir low-code-big-screen
cd low-code-big-screen
git init 
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/blueskyliu/low-code-big-screen.git
git push -u origin "master"
## 已有仓库?

cd existing_git_repo
git remote add origin https://gitee.com/blueskyliu/low-code-big-screen.git
git push -u origin "master"